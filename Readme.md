# What is this about?
This package provides an application able to visualize in real-time the continuous movement of vehicles in a Map.

This package is composed of two parts: `./api` and `./app`:
* The API notifies continuous location changes of vehicles, this is performed through Server Send Events [SSE](http://www.w3schools.com/html/html5_serversentevents.asp).
* The App is a React SPA application, which shows in real-time a google map with the vehicle's location. It connects to the `api` through [EventSource](https://developer.mozilla.org/en-US/docs/Web/API/EventSource). Note that it includes a polyfill to support EventSource on IE.

# Rationale

The original application was refactored based on the following points:
* Because the API needs only one-way-communication to notify changes in location to the browser, the best approach is through a `SSE` endpoint. `SSE` is a lightweight strategy to achieve one-way-communication still on HTTP protocol.
* WebSockets is another valid option but it is more suitable for two-ways-communication on TCP protocol. It means you may need extra configuration in your proxy or firewall.
* It uses `express` because it makes easier to create a `SSE` middleware.
* The API no longer servers a json file. It was required because of `canned` framework, but `canned` is not suitable to stream information to connected clients and it is meant to be used only on dev environment to mock rest apis.
* It uses `ES6` for client and server JS code and `ES7` property initializers on client code. It makes the code more readable, maintained and easier to write.
* It uses `webpack` in order to easily bundle the `app`.
* It covers the `api` with some unit test just as a matter of demonstration of unit-testing.

# Prerequisites
* Please check that you have `npm` and `node.js` version **^4.4.3** installed on your system and available in your path (console path).
* Please check that you have an internet connection. It is needed to interact with the google maps Api.

Note that the `nodejs` version **^4.4.3** is very important otherwise you may get build errors with new ES6 keywords like `const` or `class`.

# Intro
This package starts two servers:
* One for the `api` and runs on `http://localhost:3000`
* One for the `app` and runs on `http://localhost:8080`

Please make sure that you have those ports available.

# Starting the servers
* To start the `api` please run `npm run api`
* To start the `app` please run `npm run app`

# Continuous Integration
It uses a free hosted solution called `Codeship`. You may see the nice green Builds in the following dashboard with the given credentials:
* [Dashboard Url](https://codeship.com/projects/153876)
* User: kinexon@guest.com
* Pasword: kinexon

# Running Tests
To run only the `api` unit tests please run `npm test`

# Further Work
It would be good to use a container technology like Docker to ship the application because:

* It makes easier to ship the application to Integration/QA/Prod environments
* There are many tools to monitor and manage the containers which makes our lives easier
* It provides great isolation to the application promoting good development practices
* It allows to scale the application very easy
* It is fun !

# Disclaimer
It works on my machine :) !
This app was successfully tested in Mac and Linux using Chrome/Firefox/IE. On windows there could be problems with AdBlockers and Anti-Viruses.
In case that you have any issues, please feel free to contact me at <narg_95@hotmail.com>.

Enjoy :) !