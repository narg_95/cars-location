const sinon = require('sinon');
const should = require('should');
const SseHandler = require('./sse-handler');

describe('Component: SseHandler', () => {
    
    describe('When calling getRequestHandler', () => {
         beforeEach(function() {
            
        });
        
        it('should return a function that accepts a request and response objects', () => {
           const sseHandler = new SseHandler();
           
           const sseRequestHandler = sseHandler.getRequestHandler();
           
           sseRequestHandler.length.should.be.eql(2);
        });
        
        describe('When a new request come, that is the requestHandler is invoked', () => {
            it('should create a sseSend function in the request', () => {
               const sseHandler = new SseHandler();
               const sseRequestHandler = sseHandler.getRequestHandler();
               const request = { on: sinon.stub() };
               const response = { writeHead: sinon.stub() };
               
               sseRequestHandler(request, response);
               
               should.exist(request.sseSend);
            });
            
            it('should register a new client', () => {
               const sseHandler = new SseHandler();
               const sseRequestHandler = sseHandler.getRequestHandler();
               const request = { on: sinon.stub() };
               const response = { writeHead: sinon.stub() };
               
               sseRequestHandler.call(sseHandler, request, response);
               
               Object.keys(sseHandler.clients).length.should.be.eql(1);
            });
        });
    });
    
    describe('When broadcasting', () => {
        it('should call the client sseSend method with the given data', () => {
            const sseHandler = new SseHandler();
            const sseSend = sinon.spy();
            sseHandler.clients = { anyClient: { sseSend: sseSend } };
            const anyData = [];
            sseHandler.broadcast(anyData);
            
            sseSend.calledWith(anyData).should.be.true();
        });        
    });
});
