'use strict';

const shortid = require('shortid');

class SseHandler {

    constructor() {
        this.clients = {};
        this.sseHeaders = {
            'Content-Type': 'text/event-stream',
            'Cache-Control': 'no-cache',
            'Connection': 'keep-alive'
        };
    }

    getRequestHandler() {
        return this._handleSseRequest.bind(this)
    }

    broadcast(data) {

        Object.keys(this.clients)
            .forEach(clientId => this.clients[clientId].sseSend(data));
    }

    _handleSseRequest(request, response) {
        const clientId = shortid.generate();
        this.clients[clientId] = request;
        request.sseSend = (data) => response.write(`data: ${JSON.stringify(data)} \n\n`);
        request.on('close', () => delete this.clients[clientId]);
        response.writeHead(200, this.sseHeaders);
    }
}

module.exports = SseHandler;