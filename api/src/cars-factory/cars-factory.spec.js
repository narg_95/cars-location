const should = require('should');
const carsFactory = require('./cars-factory');

describe('Component: CarsFactory', () => {
    describe('When calling create',() => {
        it('should return an array with the given ammount of cars', () => {
            const ammount = 10; 
            const carsFactory = require('./cars-factory');
            const cars = carsFactory.create(ammount);
            
            cars.length.should.be.exactly(ammount);
        });
        
        it('should return an array with at least location and key', () => {
            const ammount = 1;
            const car = carsFactory.create(ammount)[0];
            
            should.exist(car.location);
            should.exist(car.key);
        });
    });
});