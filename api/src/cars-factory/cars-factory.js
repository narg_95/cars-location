'use strict';

const randopeep = require('randopeep');
const shortid = require('shortid');

class CarsFactory {

    create(amount) {
        return Array(amount).fill().map(() => ({
            key: shortid.generate(),
            driverName: randopeep.name(),
            driverCityOrigin: randopeep.address.city(),
            driverLanguage: ['de', 'en', 'nl', 'fr', 'es', 'ar'][Math.floor(Math.random() * 7)],
            driverPhone: randopeep.address.phone(),
            driverGender: ['male', 'female'][Math.floor(Math.random() * 2)],
            driverInfo: randopeep.corporate.catchPhrase(0),
            carMake: randopeep.corporate.name('large', 0),
            kmDriven: Math.floor(Math.random() * 100000),
            location: [48.12 + Math.random() - 0.5, 11.64 + Math.random() - 0.5]
        }));
    }
}

module.exports = new CarsFactory();