'use strict';

const carsFactory = require('../cars-factory/cars-factory');
const LocationUpdater = require('../location-updater/location-updater');
const SseHandler = require('../sse-handler/sse-handler');
const cors = require('cors');

class CarsModuleLoader {

    load(app) {
        const sseHandler = new SseHandler();
        app.use(cors());
        app.use(sseHandler.getRequestHandler());
        const cars = carsFactory.create(20);
        const intervalInSeconds = 1;
        const onCarsUpdated = (cars) => sseHandler.broadcast(cars);
        const locationUpdater = new LocationUpdater();
        
        locationUpdater.update(
            cars,
            intervalInSeconds,
            onCarsUpdated);
    }
}

module.exports = new CarsModuleLoader()