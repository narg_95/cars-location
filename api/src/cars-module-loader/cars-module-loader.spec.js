const sinon = require('sinon');
const should = require('should');
const mockery = require('mockery');

describe('Component: CarsModuleLoader', ()=> {
    
    describe('When calling load', ()=> {
        beforeEach(() => { 
            mockery.enable({
              warnOnReplace: false,
              warnOnUnregistered: false,
              useCleanCache: true
            });
        });
                    
        it('should register the Sse request handler', () => {
            const SseHandler = require('../sse-handler/sse-handler');
            const getRequestHandler= sinon.spy();
            SseHandler.prototype.getRequestHandler = getRequestHandler;
            const app = { use: sinon.stub() };
            const carModuleLoader = require('./cars-module-loader');
            
            carModuleLoader.load(app);
            
            getRequestHandler.called.should.be.true();
        });
    });
});