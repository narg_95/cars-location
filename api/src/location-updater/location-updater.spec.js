const LocationUpdater = require('./location-updater');
const sinon = require('sinon');

describe('Component: LocationUpdater', () => {
    var clock;
    
    describe('When calling update', () => {
         beforeEach(function() {
            clock = sinon.useFakeTimers();
        });
        
        afterEach( () => {
            clock.restore(); 
        });
        
        it('should call the given callback after the update', () => {
            const locationUpdater = new LocationUpdater();
            const callback = sinon.spy();
            
            locationUpdater.update([], 1, callback);
            clock.tick(1000);
            
            callback.called.should.be.true();
        });
        
        it('should update the cars location', () => {
            const locationUpdater = new LocationUpdater();
            var newLocation = [];
            var location = [0, 0];
            var car = { location: location };
            const callback = (cars) => newLocation = cars[0].location;
            
            locationUpdater.update([car], 1, callback);
            clock.tick(1000);
            
            newLocation.should.be.not.exactly(location);
        });
    });
});