'use strict';

class LocationUpdater {

    update(cars, intervalInSeconds, onCarsUpdatedCallBack) {
        this.cars = cars;
        this.onCarsUpdatedCallBack = onCarsUpdatedCallBack;
        setInterval(this._updateLocation.bind(this), intervalInSeconds * 1000);
    }

    _updateLocation() {
        this.cars = this.cars.map((car) => this._updateCarLocation(car));
        this.onCarsUpdatedCallBack(this.cars);
    }

    _updateCarLocation(car) {
        car.location = [
            this._shiftCoordinate(car.location[0], 0.001),
            this._shiftCoordinate(car.location[1], 0.0004)
        ];
        return car;
    }

    _shiftCoordinate(strCoordinate, shift) {
        var coordinate = parseFloat(strCoordinate);
        var shift = [shift, 0.0][Math.floor(Math.random() * 2)];
        return coordinate + shift;
    }
}

module.exports = LocationUpdater;