const chalk = require('chalk');
const http = require('http');
const carModuleLoader = require('./src/cars-module-loader/cars-module-loader');
const express = require('express');

const port = process.env.API_PORT || 3000;
const app = express();    

carModuleLoader.load(app);

// Create the API server
http.createServer(app).listen(port, '0.0.0.0');
console.log(chalk.green.underline.bold(`API Service running on http://localhost:${port}`));
