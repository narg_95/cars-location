const React = require('react');
const Car = require('./car.jsx');
class CarMap extends React.Component {

    static propTypes = { 
        container: React.PropTypes.object
    }

    state= {
        isFirstTime: true,
        cars: []        
    };

    componentWillMount(){
        this.carMap = new google.maps.Map(this.props.container, {});
        this.bounds = new google.maps.LatLngBounds();
        this.infoWindow = new google.maps.InfoWindow();
        
        var source = new EventSource('//localhost:3000/');
        source.addEventListener('message', event => this._onCarLocationDataArrived(event) , false);
    }
    
    _onCarLocationDataArrived(event) {
        var cars =  JSON.parse(event.data);
        cars = cars.map(car => this._UpdateCarInfo(car));                
        
        if(this.state.isFirstTime) {
            this.carMap.fitBounds(this.bounds);
            this.state.isFirstTime = false;    
        }
        
        this.setState({cars: cars});
    }
        
    _UpdateCarInfo(car){
        var location = {lat: +car.location[0], lng: +car.location[1]};
        car.location = location;
        this.bounds.extend(new google.maps.LatLng(location.lat, location.lng));
        return car;
    }
    
    render() { 
        const cars = this.state.cars;        
        return <div>{cars.map(carInfo => <Car key={carInfo.key} map={this.carMap} infoWindow={this.infoWindow} carInfo={carInfo}/>)}</div>; 
    }    
}

module.exports = CarMap;