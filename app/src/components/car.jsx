const React = require('react');
class Car extends React.Component {

    static propTypes = {
        carInfo: React.PropTypes.object,
        map: React.PropTypes.object,
        infoWindow: React.PropTypes.object
    }

    componentWillMount() {
        const carInfo = this.props.carInfo;
        this._marker = new google.maps.Marker({
            position: carInfo.location,
            map: this.props.map,
            title: carInfo.driverName,
            icon: 'http://maps.google.com/mapfiles/ms/icons/truck.png'
        });
        this._marker.addListener('click', this._showCarInfo.bind(this));
    }
    
    render() {
        this._marker.setPosition(this.props.carInfo.location);
        return null;
    }
    
    _showCarInfo() {
        const carInfo = this.props.carInfo;
        this.props.infoWindow.setContent(`<h2>${carInfo.driverName}</h2><code>${JSON.stringify(carInfo)}</code>`);
        this.props.infoWindow.open(this.props.map,this._marker);
    }
}

module.exports = Car;