const React = require('react');
const CarMap = require('./car-map.jsx');

class GoogleMapLoader extends React.Component {

    static _apiKey = 'AIzaSyCSTcUH0sqGWziKeu0zgsJccU9XiwBrX_0';

    componentWillMount() {
        const script = document.createElement("script");
        script.src = `https://maps.googleapis.com/maps/api/js?key=${GoogleMapLoader._apiKey}&callback=initMap`;
        script.async = true;
        script.defer = true;
        document.head.appendChild(script);
    }

    componentDidMount() {
        window.initMap = () => { 
            this.render = this.renderChildren;
            this.forceUpdate();
        }
    }

    render() {
        return <div className="mapContainer" ref="container"></div>;
    }
    
    renderChildren() {
         return <div className="mapContainer" ref="container">
                    <CarMap container={this.refs.container} / >
                </div>;
    }
}

module.exports = GoogleMapLoader