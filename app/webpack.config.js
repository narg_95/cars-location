var path = require("path");
var HtmlWebpackPlugin = require('html-webpack-plugin');


module.exports = {
    entry: {
        app: ["./src/app.js", "./src/components/car-map.less", "eventsource-polyfill"]
    },
    output: {
        path: path.resolve(__dirname, "build"),
        publicPath: "/",
        filename: "app.bundle.js"
    },
    module: {
        loaders: [{
            test: /.jsx?$/,
            loader: 'babel-loader',
            exclude: /node_modules/
        }, {
            test: /\.less$/,
            loader: "style!css!autoprefixer!less"
        }]
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'Cars Viewer',
            template: 'src/index.html',
            inject: 'body'
        })
    ]
};